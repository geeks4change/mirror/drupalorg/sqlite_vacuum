<?php

/**
 * Implements hook_variable_info().
 */
function sqlite_vacuum_variable_info($options) {
  $variables['sqlite_vacuum_interval'] = array(
    'title' => t('SQLite Vacuum interval'),
    'type' => 'number',
    'default' => 10800,
    'description' => t('The interval in seconds between SQLite Vacuum cron runs. Enter 0 for no cron run.'),
    'group' => 'sqlite_vacuum',
  );
  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function sqlite_vacuum_variable_group_info() {
  $groups['sqlite_vacuum'] = array(
    'title' => t('SQLite Vacuum'),
    'description' => t('Interval in seconds between cron runs of SQLite vacuum. 0 for no cron runs.'),
    'access' => 'administer sqlite vacuum',
  );
  return $groups;
}
